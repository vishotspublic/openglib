#include "extcode.h"
#include "hosttype.h"
#include <stdio.h>

#define BUFSIZE 4096 

int main(int argc, char* argv[])
{
	HANDLE handle = CreateNamedPipeW(L"\\\\.\\pipe\\opengtest", PIPE_ACCESS_DUPLEX, PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT,
								    PIPE_UNLIMITED_INSTANCES, 1024, 1024, NMPWAIT_USE_DEFAULT_WAIT, NULL);
	if (handle != INVALID_HANDLE_VALUE)
	{
		BOOL fSuccess;
		DWORD err, dwRead, dwWritten;
		CHAR chBuf[BUFSIZE];

		while (TRUE)
		{
			// Read from named pipe 
			fSuccess = ReadFile(handle, chBuf, BUFSIZE, &dwRead, NULL); 
			if (!fSuccess || dwRead == 0)
			{	 
				err = GetLastError();
				if (err != ERROR_BROKEN_PIPE)
				{
					printf("Error reading pipe, error code: %x", err);
				}
				break;          
			}

			// Write to named pipe 
			fSuccess = WriteFile(handle, chBuf, dwRead, &dwWritten, NULL); 
			if (!fSuccess) 
			{	 
				err = GetLastError();
				if (err != ERROR_BROKEN_PIPE)
				{
					printf("Error writing pipe, error code: %x", err);
				}
				break; 
			} 
		}
		CloseHandle(handle);
	}
	return 0;
}

