#include "extcode.h"
#include "hosttype.h"
#include <stdio.h> 

#if defined(DEBUG)
 #if Win32
  #if defined(_CVI_DEBUG_)
   #define DoDebugger()
  #elif _MSC_VER >= 1400
	 #define DoDebugger()   __debugbreak()
  #else
   #define DoDebugger()    {__asm int 3}
  #endif
 #elif MacOS
  #define DoDebugger()    Debugger()
 #else
  #define DoDebugger()
 #endif
 #define DEBUGPRINTF(args)      DbgPrintf args
#else
 #define DoDebugger()
 #define DEBUGPRINTF(args)
/* long DebugPrintf(char* fmt, ...);
 long DebugPrintf(char* fmt, ...)
 {
   return 0;
 }
*/
#endif

#define BUFSIZE 4096 
 
VOID main(VOID) 
{ 
    CHAR chBuf[BUFSIZE]; 
    DWORD err, dwRead, dwWritten; 
    HANDLE hStdin, hStdout, hStderr; 
    BOOL fSuccess;
 
    DoDebugger();
   
    hStdout = GetStdHandle(STD_OUTPUT_HANDLE); 
    hStdin = GetStdHandle(STD_INPUT_HANDLE); 
    hStderr = GetStdHandle(STD_ERROR_HANDLE); 
    if ((hStdout == INVALID_HANDLE_VALUE) || (hStdin == INVALID_HANDLE_VALUE))
		ExitProcess(1); 
 
    // Write welcome message to standard output.
    sprintf_s(chBuf, BUFSIZE, "Standard IO loopback test program\r\n3. Sep. 2016\r\n");
    fSuccess = WriteFile(hStdout, chBuf, (DWORD)strlen(chBuf), &dwWritten, NULL); 
    if (fSuccess) 
	{
		for (;;) 
	    { 
			// Read from standard input. 
			fSuccess = ReadFile(hStdin, chBuf, BUFSIZE, &dwRead, NULL); 
			if (!fSuccess || dwRead == 0)
			{	 
				err = GetLastError();
				if (err != ERROR_BROKEN_PIPE && hStderr != INVALID_HANDLE_VALUE)
				{
					sprintf_s(chBuf, BUFSIZE, "error = %d, dwRead = %d\r\n", err, dwRead);
					WriteFile(hStderr, chBuf, (DWORD)strlen(chBuf), &dwWritten, NULL);		  
				}
				break;          
			}

			// Write to standard output. 
			fSuccess = WriteFile(hStdout, chBuf, dwRead, &dwWritten, NULL); 
			if (!fSuccess) 
			{	 
				err = GetLastError();
				if (err != ERROR_BROKEN_PIPE && hStderr != INVALID_HANDLE_VALUE)
				{
					sprintf_s(chBuf, BUFSIZE, "error = %d, dwRead = %d\r\n", err, dwRead);
					WriteFile(hStderr, chBuf, (DWORD)strlen(chBuf), &dwWritten, NULL);		  
				}
				break; 
			}
		}
	}
	else
	{
		ExitProcess(2); 
	}
}