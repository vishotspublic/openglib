/*
 * Pipe shared library for LabVIEW
 *
 * Copyright (C) 2004-2017 Rolf Kalbermatter, rolf.kalbermatter@kalbermatter.nl
 *
 * Please visit https://lavag.org/forum/45-openg/ to learn about the
 * Open Source LabVIEW software movement.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <signal.h>
#include "extcode.h"
#include "hosttype.h"
#include "pipes.h"


#if defined(MSWin)
#define STANDARD_IN     STD_INPUT_HANDLE
#define STANDARD_OUT    STD_OUTPUT_HANDLE 
#define STANDARD_ERR    STD_ERROR_HANDLE
typedef HANDLE std_handle;
typedef HANDLE pipe_handle; 
static MgErr Win32ToLVErr(DWORD error);
#elif defined (Unix)
#define STANDARD_IN     0
#define STANDARD_OUT    1
#define STANDARD_ERR    2

typedef struct file_ref std_handle;
typedef int pipe_handle; 

static MgErr UnixToLVErr(int32 errno);
#elif defined (Mac)
static MgErr MacToLVErr(OSErr osErr);
#endif

static MgErr RedirectStandardIO(int32 type, std_handle *stdIO, pipe_handle *rmpipe, LVRefNum *refnum);
static MgErr RestoreStandardIO(int32 type, std_handle *stdIO, pipe_handle rmpipe, LVRefNum *refnum, MgErr err);
static MgErr CreateChildProcess(LPSTR username, LPSTR password, LPSTR cmdline, pipe_handle stdIn, pipe_handle stdOut, pipe_handle stdErr, uInt32 *pid);

static MgErr createRefnum(pipe_handle fd, LVRefNum *refnum);
static MgErr accessRefnum(LVRefNum refnum, pipe_handle *fd);
static MgErr closeRefnum(LVRefNum refnum);


LibAPI(MgErr) PipeOpen(CStr name, uInt16 mode, LVRefNum *refnum)
{
	MgErr err = noErr;
#if defined(MSWin)
	HANDLE handle;
	DWORD dwMode;
#elif defined(Unix)
	mode_t mode;
	int fd;
#endif
	*refnum = kNotAMagicCookie;

	DoDebugger();

	if (mode > kReadWriteMode)
		return mgArgErr;

#if defined(MSWin)
	if (WaitNamedPipe(name, 1000))
	{
		switch (mode)
		{
			case kReadMode:
				dwMode = GENERIC_READ;
				break;
			case kWriteMode:
				dwMode = GENERIC_WRITE;
				break;
			case kReadWriteMode:
				dwMode = GENERIC_READ | GENERIC_WRITE;
				break;
		}

		/* There is a server to connect to, so connect as client */
		handle = CreateFile(name, dwMode, 0, NULL, OPEN_EXISTING, 0 , NULL);
	}
	else
	{
		switch (mode)
		{
			case kReadMode:
				dwMode = PIPE_ACCESS_INBOUND;
				break;
			case kWriteMode:
				dwMode = PIPE_ACCESS_OUTBOUND;
				break;
			case kReadWriteMode:
				dwMode = PIPE_ACCESS_DUPLEX;
				break;
		}

		/* There is no server to connect to, so create our own server */
		handle = CreateNamedPipe(name, dwMode, PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT,
								 PIPE_UNLIMITED_INSTANCES, 1024, 1024, NMPWAIT_USE_DEFAULT_WAIT, NULL);
	}
	if (handle == INVALID_HANDLE_VALUE)
	{
		err = Win32ToLVErr(GetLastError());
	}
	else
	{
		err = createRefnum(handle, refnum);
		if (err)
			CloseHandle(handle);
	}
#elif defined(Unix)
	switch (mode)
	{
		case kReadMode:
			mode = O_RDONLY;
			break;
		case kWriteMode:
			mode = O_WRONLY;
			break;
		case kReadWriteMode:
			mode = O_RDWR;
			break;
	}

#if OpSystem == VxWorks
	if (pipeDevCreate(name, 100, 100) < 0)
#else
	/* Attempt to create the named pipe */
	if (mkfifo(name, S_IWUSR | S_IRUSR | S_IRGRP | S_IWGRP | S_IROTH) < 0)
#endif
	{
		err = UnixToLVErr(errno);
	}

	if (!err || err == fDupPath)
	{
		err = noErr;
		fd = open(name, mode | O_NONBLOCK);
		if (fd < 0)
		{
			err = UnixToLVErr(errno);
		}
	}

	if (!err)
	{
		err = createRefnum(fd, refnum);
		if (err)
			close(fd);
	}
#else
	err = mgNotImplementd:
#endif
	return err;
}


LibAPI(MgErr) PipeOpenCmd(CStr cmd, uInt16 mode, LVRefNum *refnumIn, LVRefNum *refnumOut, LVRefNum *refnumErr, uInt32 *processID)
{
	return PipeOpenCmdAsUser(NULL, NULL, cmd, mode, refnumIn, refnumOut, refnumErr, processID);
}

LibAPI(MgErr) PipeOpenCmdAsUser(CStr username, CStr password, CStr cmd, uInt16 mode, LVRefNum *refnumIn, LVRefNum *refnumOut, LVRefNum *refnumErr, uInt32 *processID)
{
	MgErr err = dvOpenErr;
#if defined(MSWin)
	SECURITY_ATTRIBUTES saAttr = { 0 };
	HANDLE hSaveStdout = 0L, hChildStdout = 0L;
	HANDLE hSaveStdin = 0L, hChildStdin = 0L;
	HANDLE hSaveStderr = 0L, hChildStderr = 0L;
#elif defined(Unix)
	std_handle hSaveStdout = 0L, hSaveStdin = 0L, hSaveStderr = 0L;
	pipe_handle hChildStdout = 0L, hChildStdin = 0L, hChildStderr = 0L;
#endif
	if ((mode > kReadWriteMode) ||
		((mode == kReadMode || mode == kReadWriteMode) && !refnumIn) ||
		((mode == kWriteMode || mode == kReadWriteMode) && !refnumOut))
		return mgArgErr;

	DoDebugger();

	if (mode == kReadMode || mode == kReadWriteMode)
	{
		*refnumIn = kNotAMagicCookie;
		err = RedirectStandardIO(STANDARD_IN, &hSaveStdin, &hChildStdin, refnumIn);
		if (err)
			goto out;
	}

	if (mode == kWriteMode || mode == kReadWriteMode)
	{
		*refnumOut = kNotAMagicCookie;
		err = RedirectStandardIO(STANDARD_OUT, &hSaveStdout, &hChildStdout, refnumOut);
		if (err)
			goto out;
	}

	if (refnumErr)
	{
		*refnumErr = kNotAMagicCookie;
		err = RedirectStandardIO(STANDARD_ERR, &hSaveStderr, &hChildStderr, refnumErr);
		if (err)
			goto out;
	}
	
 	/* Now create the child process. */
	err = CreateChildProcess(username, password, cmd, hChildStdin, hChildStdout, hChildStderr, processID);

out:
	if (processID && err)
		*processID = 0;

	/* After process creation, restore the saved STDIN. STDOUT,
	   and STDERR. Also close the intermediate handle. */
	if (mode == kReadMode || mode == kReadWriteMode)
	{
		RestoreStandardIO(STANDARD_IN, &hSaveStdin, hChildStdin, refnumIn, err);
    }

	if (mode == kWriteMode || mode == kReadWriteMode)
	{
		RestoreStandardIO(STANDARD_OUT, &hSaveStdout, hChildStdout, refnumOut, err);
	}

	if (refnumErr && *refnumErr)
	{
		RestoreStandardIO(STANDARD_ERR, &hSaveStderr, hChildStderr, refnumErr, err);
	}
	return err;
}

LibAPI(MgErr) ExecuteCommand(CStr command, CStr verb, CStr param, uInt32 *processID)
{
	MgErr err = noErr;
#if defined(MSWin)
	err = (int32)ShellExecuteA(NULL, verb, command, param[0] ? param : NULL, NULL, SW_HIDE);
	if (err <= 32)
		err = Win32ToLVErr(err);
	else
		err = noErr;
#elif Unix
	err = mgNotSupported;
#endif
	return err;
}

LibAPI(MgErr) KillProcess(uInt32 pid, int32 exitCode)
{
	MgErr err = noErr;
#if defined(MSWin)
	HANDLE handle;
	HANDLE hToken;
	LUID sedebugnameValue;
	TOKEN_PRIVILEGES tkp;
#endif

	if (!pid)
		return mgArgErr;

#if defined(MSWin)

	/* try to acquire SeDebugPrivilege */
    if (OpenProcessToken(GetCurrentProcess(),
		TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
	{
		if (LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &sedebugnameValue))
		{
			tkp.PrivilegeCount = 1;
			tkp.Privileges[0].Luid = sedebugnameValue;
			tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
			AdjustTokenPrivileges(hToken, FALSE, &tkp, sizeof tkp, NULL, NULL);
		}
	}
	CloseHandle(hToken);
	
	handle = OpenProcess(PROCESS_TERMINATE, FALSE, pid);
	if (!handle)
		return Win32ToLVErr(GetLastError());

	if (!TerminateProcess(handle, (UINT)exitCode))
		err = Win32ToLVErr(GetLastError());
	CloseHandle(handle);
#elif defined(Unix)
	kill(pid, SIGKILL);
#else
	err = mgNotImplementd;
#endif
	return err;
}

LibAPI(MgErr) PipeClose(LVRefNum *refnum)
{
	pipe_handle fd;
	MgErr err = accessRefnum(*refnum, &fd);
	RTSetCleanupProc((CleanupProcPtr)&closeRefnum, (UPtr)*refnum, kCleanRemove);
	if (!err)
	{
		err = closeRefnum(*refnum);
	}
	return err;
}

LibAPI(MgErr) PipeRead(LVRefNum *refnum, uInt32 offset, uInt32 *bytesRead, uInt16 mode, LStrHandle data, uInt32 *eof, LVRefNum *context)
{
	pipe_handle fd;
	MgErr err = accessRefnum(*refnum, &fd);
	uInt32 bytesReq = *bytesRead;
#if defined(MSWin)
	DWORD ret;
#elif Unix
	fd_set fdset;
	timeval timeout;
	int32 ret;
#endif

	if (err)
	{
		*bytesRead = 0;
		return err;
    }

#if defined(MSWin)
    /* ReadFile blocks on any pipe not opened in overlapped mode and the anonymous
       pipes used for standard handle redirection are always non-overlapping. We don't
       want to block here, as for LabVIEW configurations with only one thread per
       execution system we would block the ReadPipe function and can't read another
       pipe. So we peek first at the pipe to see if there is anything available and if so
       we invoke ReadFile to read the entire data instead of maybe only part of a byte-
       stream sent from the other side.*/
	if (!PeekNamedPipe(fd, NULL, 0, NULL, bytesRead, NULL))
	{
		ret = GetLastError();
		*eof = (ret == ERROR_HANDLE_EOF || ret == ERROR_BROKEN_PIPE);
		if (!*eof)
			err = Win32ToLVErr(ret);
		*bytesRead = 0;
	}
	else if (*bytesRead)
    {
		/* There are some bytes to read, so try to read as much as possible now! */
		if (!ReadFile(fd, LStrBuf(*data) + offset, bytesReq - offset, bytesRead, NULL))
		{
			ret = GetLastError();
			*eof = (ret == ERROR_HANDLE_EOF || ret == ERROR_BROKEN_PIPE);
			if (!*eof)
			{
				if (ret == ERROR_MORE_DATA)
				{
					/* Should we try to read more data than requested? */
				}
				else
				{
					err = Win32ToLVErr(ret);
				}
			}
		}
    }
#elif defined(Unix)
    FD_ZERO(&fdset)
	FD_SET(fd, &fdset)

	ret = select(fd, &fdset, NULL, NULL, timeout);

	*bytesRead = read(fd, LStrBuf(*data) + offset, bytesReq - offset)
	if (*bytesRead < 0)
	{
		*eof = (errno == EEOF);
		if (!*eof)
		{
			err = UnixToLVErr(errno);
			*bytesRead = 0;
		}
	}
#else
	err = mgNotImplementd;
#endif
	return err;
}

LibAPI(MgErr) PipeWrite(LVRefNum *refnum, uInt32 offset, uInt32 *bytesWritten, LStrHandle data)
{
	pipe_handle fd;
	MgErr err = accessRefnum(*refnum, &fd);

	if (err)
		return err;

	if (!bytesWritten || !data)
		return mgArgErr;

#if defined(MSWin)
	if (!WriteFile(fd, LStrBuf(*data) + offset, *bytesWritten - offset, bytesWritten, NULL) )
		err = Win32ToLVErr(GetLastError());

#elif defined(Unix)
	*bytesWritten = write(fd, LStrBuf(*data) + offset, *bytesWritten - offset);
	if (*bytesWritten < 0)
	{
		*bytesWritten = 0;
		err = UnixToLVErr(errno);
	}
#else
	err = mgNotImplementd;
#endif
	return err;
}

static const WORD MAX_CONSOLE_LINES = 500;

LibAPI(MgErr) InitializeStandardIO(uInt32 processID, LVRefNum *refnumIn, LVRefNum *refnumOut, LVRefNum *refnumErr)
{
    MgErr err = noErr;
#if defined(MSWin)
	BOOL retVal;
	int hConHandle;
	HANDLE stdHandle;
	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	FILE *fp;

	if (processID)
	{
		retVal = AttachConsole(processID);
		if (!retVal)
		{
			// if we called AttachConsole() and there was an ERROR_ACCESS_DENIED error, we already had
			// a console and can continue with that
			err = Win32ToLVErr(GetLastError());
			if (err && err != fNoPerm)
				return err;
		}
	}
	else
	{
		// allocate a console for this app, if we already created one then the function will return with an error
		retVal = AllocConsole();
		if (!retVal)
		{
			err = Win32ToLVErr(GetLastError());
		}
	}	

	stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (stdHandle == INVALID_HANDLE_VALUE || stdHandle == NULL)
	{
		// handle could not be retrieved or console doesn't have a standard output handle
		if (!err)
			FreeConsole();
		return fIOErr;
	}

	// set the screen buffer to be big enough to let us scroll text
	GetConsoleScreenBufferInfo(stdHandle, &coninfo);
	coninfo.dwSize.Y = MAX_CONSOLE_LINES;
	SetConsoleScreenBufferSize(stdHandle, coninfo.dwSize);

	if (refnumOut)
	{
		*refnumOut = kNotAMagicCookie;

		// redirect unbuffered STDOUT to the console
		hConHandle = _open_osfhandle((intptr_t)stdHandle, _O_TEXT);
		if (hConHandle != -1)
		{
			fp = _fdopen(hConHandle, "w");
			if (fp != 0)
			{
				setvbuf(fp, NULL, _IONBF, 0);
			}
		}
		err = createRefnum(stdHandle, refnumOut);
	}

	if (refnumIn)
	{
		*refnumIn = kNotAMagicCookie;
	
		// redirect unbuffered STDIN to the console
		stdHandle = GetStdHandle(STD_INPUT_HANDLE);
		if (stdHandle != INVALID_HANDLE_VALUE && stdHandle != NULL)
		{
			hConHandle = _open_osfhandle((intptr_t)stdHandle, _O_TEXT);
			if (hConHandle != -1)
			{
				fp = _fdopen(hConHandle, "r");
				if (fp != 0)
				{
					setvbuf(fp, NULL, _IONBF, 0);
				}
			}
			err = createRefnum(stdHandle, refnumIn);
		}
	}

	if (refnumErr)
	{
		*refnumErr = kNotAMagicCookie;
	
		// redirect unbuffered STDERR to the console
		stdHandle = GetStdHandle(STD_ERROR_HANDLE);
		if (stdHandle != INVALID_HANDLE_VALUE && stdHandle != NULL)
		{
			hConHandle = _open_osfhandle((intptr_t)stdHandle, _O_TEXT);
			if (hConHandle != -1)
			{
				fp = _fdopen(hConHandle, "w");
				if (fp != 0)
				{
					setvbuf(stderr, NULL, _IONBF, 0);
				}
			}
			err = createRefnum(stdHandle, refnumErr);
		}
	}
	// make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog
	// point to console as well
//	ios::sync_with_stdio();
#elif Unix

#endif
	return noErr;
}

static MgErr RedirectStandardIO(int32 type, std_handle *stdIO, pipe_handle *rmpipe, LVRefNum *refnum)
{
	pipe_handle lvpipe;
    MgErr err = noErr;
#if defined(MSWin)
	SECURITY_ATTRIBUTES saAttr = { 0 };
    HANDLE temp;
    BOOL ret;

	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.lpSecurityDescriptor = NULL;
	saAttr.bInheritHandle = TRUE;

	/* Save the handle to the current STDxx. */
	*stdIO = GetStdHandle(type);
    if (*stdIO ==  INVALID_HANDLE_VALUE)
		return Win32ToLVErr(GetLastError());

	/* Create a pipe for the child process's standard IO. */
	if (type == STANDARD_IN)
		ret = CreatePipe(rmpipe, &temp, &saAttr, 0);
	else
		ret = CreatePipe(&temp, rmpipe, &saAttr, 0);

    if (ret)
	{
		/* Set the handle to the pipe to be STD IO. */
		ret = SetStdHandle(type, *rmpipe);
		if (ret)
		{
			/* Duplicate the LabVIEW side handle to the pipe so it is not
			   inherited by the child process. */
			ret = DuplicateHandle(GetCurrentProcess(), temp, 
	                              GetCurrentProcess(), &lvpipe, 0,
	                              FALSE, DUPLICATE_SAME_ACCESS);
			if (ret)
			{
				err = createRefnum(lvpipe, refnum);
			}
		}
		CloseHandle(temp);
	}
    if (!ret)
		err = Win32ToLVErr(GetLastError());
#elif defined(Unix)
	struct pipe_ref p_fd;
    pipe_handle temp;
    int i;

	/* save current std input fd + flags, and set not inheritable */
	if (stdio->handle = dup(type) < 0)
		return UnixToLVErr(errno);

    stdio->flags = fcntl(type, F_GETFD, 0);
	fcntl(stdio->handle, F_SETFD, stdio->flags | FD_CLOEXEC);

	if (pipe((int *)&p_fd))
    {
		err = UnixToLVErr(errno);
		close(stdio->handle);
		return err;
    }
	if (type == STANDARD_IN)
	{
		temp = p_fd.wr;
		*rmpipe = p_fd.rd;
	}
	else
	{
		temp = p_fd.rd;
		*rmpipe = p_fd.wr;
	}

    if (dup2(*rmpipe, type) == type)
	{
		close(*rmpipe);
		i = fcntl(temp, F_GETFD, 0);
		fcntl(temp, F_SETFD, i | FD_CLOEXEC);

		if ((lvpipe = (uInt32)fdopen(temp, type == STANDARD_IN ? "wb" : "rb")) == NULL)
		{
			err = UnixToLVErr(errno);
			close(stdio->handle);
			close(temp);
		}
		else
		{
 			err = createRefnum(lvpipe, refnum);
		}
    }
	else
	{
	}
#else
    err = mgNotImplemented
#endif
    return err;
}

static MgErr RestoreStandardIO(int32 type, std_handle *stdIO, pipe_handle rmpipe, LVRefNum *lvpipe, MgErr err)
{
	pipe_handle fd;
#if defined(MSWin)
	if (rmpipe)
		CloseHandle(rmpipe);
	SetStdHandle(type, *stdIO);
	if (err)
	{
		RTSetCleanupProc((CleanupProcPtr)&closeRefnum, (UPtr)*lvpipe, kCleanRemove);
		err = accessRefnum(*lvpipe, &fd);
		if (!err)
			CloseHandle(fd);
		closeRefnum(*lvpipe);
		*lvpipe = kNotAMagicCookie;
	}
#elif defined(Unix)
	if (rmpipe)
		close(rmpipe);
	fcntl(stdIO->handle, F_SETFD, stdIO->flags);
    dup2(stdIO->handle, type);
	if (err)
	{
		RTSetCleanupProc((CleanupProcPtr)&closeRefnum, (UPtr)*lvpipe, kCleanRemove);
		err = accessRefnum(*lvpipe, &fd);
		if (!err)
			close(fd);
		closeRefnum(*lvpipe);
		*lvpipe = kNotAMagicCookie;
	}
#else
    return mgNotImplementd
#endif
	return err;
}

#if defined(MSWin)
static MgErr MultibyteToWide(LPSTR sstr, int slen, LPWSTR *dstr)
{
	int dlen = MultiByteToWideChar(CP_ACP, 0, sstr, slen, NULL, 0);
	if (!dstr || dlen <= 0)
	{
		return mgArgErr;
	}
	
	*dstr = malloc(sizeof(WCHAR) * (dlen + 1));
	if (!*dstr)
	{
		return mFullErr;
	}
	dlen = MultiByteToWideChar(CP_ACP, 0, sstr, slen, *dstr, dlen + 1);
	return noErr;
}
#endif

static MgErr CreateChildProcess(LPSTR username, LPSTR password, LPSTR cmdline, pipe_handle stdIn, pipe_handle stdOut, pipe_handle stdErr, uInt32 *pid)
{
	MgErr err = noErr;
#if defined(MSWin)
	PROCESS_INFORMATION piProcInfo; 
	STARTUPINFOW siStartInfo;
	LPWSTR cmdW = NULL;
	BOOL bFuncRetn = FALSE;

	err = MultibyteToWide(cmdline, -1, &cmdW);
	if (err)
		return err;

	/* Set up members of the PROCESS_INFORMATION structure. */
	ZeroMemory(&piProcInfo, sizeof(PROCESS_INFORMATION));

	/* Set up members of the STARTUPINFO structure. */
	ZeroMemory(&siStartInfo, sizeof(STARTUPINFOW));
	siStartInfo.cb = sizeof(STARTUPINFO);
	if (stdIn || stdOut || stdErr)
		siStartInfo.dwFlags = STARTF_USESTDHANDLES;

	if (pid && *pid)
		siStartInfo.dwFlags |= STARTF_USESHOWWINDOW;
    siStartInfo.wShowWindow = SW_HIDE;

	siStartInfo.hStdInput = stdIn;
    siStartInfo.hStdOutput = stdOut;
    siStartInfo.hStdError = stdErr;

	/* Create the child process. */
	if (username && *username)
	{
		LPWSTR userW = NULL, passW = NULL;
		err = MultibyteToWide(username, -1, &userW);
		if (!err && password && *password)
		{
			err = MultibyteToWide(password, -1, &passW);
		}
		if (!err)
		{
			bFuncRetn = CreateProcessWithLogonW(
				  userW,         // username@domain
				  NULL,
				  passW,         // password
				  LOGON_WITH_PROFILE,
				  NULL,          // application name
				  cmdW,          // command line 
			      0,             // creation flags 
				  NULL,          // use parent's environment 
			      NULL,          // use parent's current directory 
			      &siStartInfo,  // STARTUPINFO pointer 
				  &piProcInfo);  // receives PROCESS_INFORMATION 
		}
		if (userW)
			free(userW);
		if (passW)
			free(passW);
	}
	else
	{
		bFuncRetn = CreateProcessW(
			  NULL,          // application name
			  cmdW,          // command line 
		      NULL,          // process security attributes 
			  NULL,          // primary thread security attributes 
		      TRUE,          // handles are inherited 
		      0,             // creation flags 
			  NULL,          // use parent's environment 
		      NULL,          // use parent's current directory 
		      &siStartInfo,  // STARTUPINFO pointer 
			  &piProcInfo);  // receives PROCESS_INFORMATION 
	}
	free(cmdW);
	if (!bFuncRetn) 
		return err ? err : mFullErr;

	if (pid)
		*pid = piProcInfo.dwProcessId;
	CloseHandle(piProcInfo.hProcess);
	CloseHandle(piProcInfo.hThread);
#elif defined(Unix)
	int pi, i = 1;
	char *argv[64];

	/* We can't really support execution under a different user easily */
	if (username || *username || password || *password)
		return mgNotSupported;

	/* parse command line into arguments */
    while (*cmdline != '\0' && i < 64)
	{ 
		/* if not the end of cmdline ..... */ 
		while (*cmdline == ' ' || *cmdline == '\t' || 
			   *cmdline == '\n' || *cmdline == '\r')
        *cmdline++ = '\0';    /* replace white spaces with 0    */
		*argv++ = cmdline;      /* save the argument position     */
		while (*cmdline != '\0' && *cmdline != ' ' && 
               *cmdline != '\t' && *cmdline != '\n' && *cmdline != '\r') 
        cmdline++;            /* skip the argument until ...    */
		i++;
	}
    *argv = '\0';          /* mark the end of argument list  */

	/* spawn the child process */
	pi = fork();
	
	if (pi == 0 )
	{
		/* This is the child process. Execute the command */
		execvp(args[0], args);
		_exit(1);
	}
	else if (pi == -1 )
	{	
		/* fork failed */
		if (pid)
			*pid = 0L;
		err = dvOpenErr;
	}
	else if (pid)
		*pid = pi;
#else
    return mgNotImplementd
#endif
	return err;
}

static MagicCookieJar gRefnumJar = NULL;

static MgErr createRefnum(pipe_handle fd, LVRefNum *refnum)
{
	if (!gRefnumJar)
		gRefnumJar = MCNewBigJar(sizeof(pipe_handle));
	if (gRefnumJar)
	{
		*refnum = MCNewCookie(gRefnumJar, (MagicCookieInfo)&fd);
		if (*refnum)
		{
			RTSetCleanupProc((CleanupProcPtr)&closeRefnum, (UPtr)*refnum, kCleanOnIdle);
			return noErr;
		}
	}	
	return mFullErr;
}

static MgErr accessRefnum(LVRefNum refnum, pipe_handle *fd)
{
	return MCGetCookieInfo(gRefnumJar, refnum, (MagicCookieInfo)fd);
}

static MgErr closeRefnum(LVRefNum refnum)
{
	pipe_handle fd;
	MgErr err = MCDisposeCookie(gRefnumJar, refnum, (MagicCookieInfo)&fd);
	if (!err)
	{
#if defined(MSWin)
		if (!CloseHandle(fd))
			err = Win32ToLVErr(GetLastError());

#elif defined(Unix)
		if (close(fd) < 0)
			err = UnixToLVErr(errno);
#else
		err = mgNotImplementd
#endif
	}
	return err;
}

/* size of the table */
#define ERRTABLESIZE (sizeof(errtable)/sizeof(errtable[0]))

#if defined(MSWin)
struct errentry {
	DWORD oscode;         /* OS return value */
	MgErr err;            /* LabVIEW error code */
};

static struct errentry errtable[] = {
	{ERROR_INVALID_FUNCTION,		mgArgErr  },   /* 1 */
	{ERROR_FILE_NOT_FOUND,			fNotFound },   /* 2 */
	{ERROR_PATH_NOT_FOUND,			fNotFound },   /* 3 */
	{ERROR_TOO_MANY_OPEN_FILES,		fTMFOpen  },   /* 4 */
	{ERROR_ACCESS_DENIED,			fNoPerm   },   /* 5 */
	{ERROR_INVALID_HANDLE,			mgArgErr  },   /* 6 */
	{ERROR_ARENA_TRASHED,			mFullErr  },   /* 7 */
	{ERROR_NOT_ENOUGH_MEMORY,		mFullErr  },   /* 8 */
	{ERROR_INVALID_BLOCK,			mFullErr  },   /* 9 */
	{ERROR_BAD_ENVIRONMENT,			fNotEnabled }, /* 10 */
	{ERROR_BAD_FORMAT,				fNotEnabled }, /* 11 */
	{ERROR_INVALID_ACCESS,			mgArgErr  },   /* 12 */
	{ERROR_INVALID_DATA,			mgArgErr  },   /* 13 */
	{ERROR_INVALID_DRIVE,			fNotFound },   /* 15 */
	{ERROR_CURRENT_DIRECTORY,		fNoPerm   },   /* 16 */
	{ERROR_NOT_SAME_DEVICE,			fIOErr    },   /* 17 */
	{ERROR_NO_MORE_FILES,			fNotFound },   /* 18 */
	{SE_ERR_SHARE,                  fNoPerm   },   /* 26 */
	{SE_ERR_ASSOCINCOMPLETE,		fNotFound },   /* 27 */
	{SE_ERR_DDETIMEOUT,				fIOErr    },   /* 28 */
	{SE_ERR_DDEFAIL,				fIOErr    },   /* 29 */
	{SE_ERR_DDEBUSY,				fIOErr    },   /* 30 */
	{SE_ERR_NOASSOC,                fNotFound },   /* 31 */
	{SE_ERR_DLLNOTFOUND,            fNotFound },   /* 32 */
	{ERROR_LOCK_VIOLATION,			fNoPerm   },   /* 33 */
	{ERROR_BAD_NETPATH,				fNotFound },   /* 53 */
	{ERROR_NETWORK_ACCESS_DENIED,	fNoPerm   },   /* 65 */
	{ERROR_BAD_NET_NAME,			fNotFound },   /* 67 */
	{ERROR_FILE_EXISTS,				fDupPath  },   /* 80 */
	{ERROR_CANNOT_MAKE,				fNoPerm   },   /* 82 */
	{ERROR_FAIL_I24,				fNoPerm   },   /* 83 */
	{ERROR_INVALID_PARAMETER,		mgArgErr  },   /* 87 */
	{ERROR_NO_PROC_SLOTS,			mFullErr  },   /* 89 */
	{ERROR_DRIVE_LOCKED,			fNoPerm   },   /* 108 */
	{ERROR_BROKEN_PIPE,				fIOErr    },   /* 109 */
	{ERROR_DISK_FULL,				fDiskFull },   /* 112 */
	{ERROR_INVALID_TARGET_HANDLE,	mgArgErr  },   /* 114 */
	{ERROR_WAIT_NO_CHILDREN,		fNotEnabled }, /* 128 */
	{ERROR_CHILD_NOT_COMPLETE,		fNotEnabled }, /* 129 */
	{ERROR_DIRECT_ACCESS_HANDLE,	mgArgErr  },   /* 130 */
	{ERROR_NEGATIVE_SEEK,			mgArgErr  },   /* 131 */
	{ERROR_SEEK_ON_DEVICE,			fNoPerm   },   /* 132 */
	{ERROR_DIR_NOT_EMPTY,			fNoPerm   },   /* 145 */
	{ERROR_NOT_LOCKED,				fNoPerm   },   /* 158 */
	{ERROR_BAD_PATHNAME,			fNotFound },   /* 161 */
	{ERROR_MAX_THRDS_REACHED,		mFullErr  },   /* 164 */
	{ERROR_LOCK_FAILED,				fNoPerm   },   /* 167 */
	{ERROR_ALREADY_EXISTS,			fDupPath  },   /* 183 */
	{ERROR_FILENAME_EXCED_RANGE,	fNotFound },   /* 206 */
	{ERROR_NESTING_NOT_ALLOWED,		mFullErr  },   /* 215 */
	{ERROR_PIPE_BUSY,				ncBusyErr },   /* 231 */
	{ERROR_NO_DATA,					fEOF      },   /* 232 */
	{ERROR_PIPE_NOT_CONNECTED,		ncNotConnectedErr }, /* 233 */
	{ERROR_MORE_DATA,				ncInProgress }, /* 234 */
	{ERROR_NOT_ENOUGH_QUOTA,		mFullErr  }    /* 1816 */
};

/* The following two constants must be the minimum and maximum
   values in the (contiguous) range of Exec Failure errors. */
#define MIN_EXEC_ERROR ERROR_INVALID_STARTING_CODESEG
#define MAX_EXEC_ERROR ERROR_INFLOOP_IN_RELOC_CHAIN

/* These are the low and high value in the range of errors that are
   access violations */
#define MIN_EACCES_RANGE ERROR_WRITE_PROTECT
#define MAX_EACCES_RANGE ERROR_SHARING_BUFFER_EXCEEDED

static MgErr Win32ToLVErr(DWORD error)
{
	int i;

	/* check the table for the OS error code */
	for (i = 0; i < ERRTABLESIZE; ++i)
	{
		if (error == errtable[i].oscode)
		{
			return errtable[i].err;
		}
	}

	/* The error code wasn't in the table.  We check for a range of */
	/* EACCES errors or exec failure errors (ENOEXEC).  Otherwise   */
	/* EINVAL is returned.                                          */

	if (error >= MIN_EACCES_RANGE && error <= MAX_EACCES_RANGE)
		return fNoPerm;
	else if (error >= MIN_EXEC_ERROR && error <= MAX_EXEC_ERROR)
		return mgArgErr;
	return HRESULT_FROM_WIN32(error);
}

#elif defined(Unix)
struct errentry {
	int32 errnocode;      /* System V error code */
	MgErr err;            /* LabVIEW error code */
};

static struct errentry errtable[] = {
	{EINVAL,     mgArgErr  },
	{ENXIO,      fNotFound },
	{ENOENT,     fNotFound },
	{EMFILE,     fTMFOpen  },
	{EACCES,     fNoPerm   },
	{EBADF,      mgArgErr  },
	{ENOMEM,     mFullErr  },
	{E2BIG,      fNotEnabled },
	{ENOEXEC,    fNotEnabled },
	{EXDEV,      fIOErr    },
	{EEXIST,     fDupPath  },
	{EAGAIN,     mFullErr  },
	{EPIPE,      fIOErr    },
	{ENOSPC,     fDiskFull },
	{ECHILD,     fNotEnabled },
	{ENOTEMPTY,  fNoPerm   },
};

static MgErr UnixToLVErr(int32 errno)
{
	int i;

	/* check the table for the OS error code */
	for (i = 0; i < ERRTABLESIZE; ++i)
	{
		if (errno == errtable[i].errnocode)
		{
			return errtable[i].err;
		}
	}

	/* The error code wasn't in the table. */
	return bogusError;
}

#elif defined (Mac)
struct errentry {
	OSErr maccode;        /* MAC return value */
	MgErr err;            /* LabVIEW error code */
};

static struct errentry errtable[] = {
	{ioErr,        fIOErr    },     /*I/O error (bummers)*/
	{paramErr,     mgArgErr  },     /*error in user parameter list */
	{dirFulErr,    fDiskFull },     /*Directory full*/
	{dskFulErr,    fDiskFull },     /*disk full*/
	{nsvErr,       fNotFound },     /*no such volume*/
	{bdNamErr,     mgArgErr  },     /*there may be no bad names in the final system!*/
	{fnOpnErr,     fNoPerm   },     /*File not open*/
	{eofErr,       fEOF      },     /*End of file*/
	{posErr,       mgArgErr  },     /*tried to position to before start of file (r/w)*/
	{mFulErr,      mFullErr  },     /*memory full (open) or file won't fit (load)*/
	{tmfoErr,      fTMFOpen  },     /*too many files open*/
	{fnfErr,       fNotFound },     /*File not found*/
	{wPrErr,       fNoPerm   },     /*diskette is write protected.*/
	{fLckdErr,     fNoPerm   },     /*file is locked*/
	{vLckdErr,     fNoPerm   },     /*volume is locked*/
	{fBsyErr,      fNoPerm   },     /*File is busy (delete)*/
	{dupFNErr,     fDupPath  },     /*duplicate filename (rename)*/
	{opWrErr,      fNoPerm   },     /*file already open with with write permission*/
	{rfNumErr,     mgArgErr  },     /*refnum error*/
	{gfpErr,       mgArgErr  },     /*get file position error*/
	{volOffLinErr, mgArgErr  },     /*volume not on line error (was Ejected)*/
	{permErr,      fNoPerm   },     /*permissions error (on file open)*/
	{volOnLinErr,  mgArgErr  },     /*drive volume already on-line at MountVol*/
	{nsDrvErr,     mgArgErr  },     /*no such drive (tried to mount a bad drive num)*/
	{noMacDskErr,  mgArgErr  },     /*not a mac diskette (sig bytes are wrong)*/
	{extFSErr,     mgArgErr  },     /*volume in question belongs to an external fs*/
	{fsRnErr,      mgArgErr  },     /*file system internal error:during rename the old
	                                  entry was deleted but could not be restored.*/
	{badMDBErr,    mgArgErr  },     /*bad master directory block*/
	{wrPermErr,    fNoPerm   },     /*write permissions error*/
	{dirNFErr,     fNotFound },     /*Directory not found*/
	{tmwdoErr,     fTMFOpen  },     /*No free WDCB available*/
	{badMovErr,    mgArgErr  },     /*Move into offspring error*/
	{wrgVolTypErr, mgArgErr  },     /*Wrong volume type error [operation not supported for MFS]*/
	{volGoneErr,   mgArgErr  },     /*Server volume has been disconnected.*/
	{fidNotFound,  mgArgErr  },     /*no file thread exists.*/
	{fidExists,    fDupPath  },     /*file id already exists*/
	{notAFileErr,  mgArgErr  },     /*directory specified*/
	{diffVolErr,   mgArgErr  },     /*files on different volumes*/
	{catChangedErr, mgArgErr  },    /*the catalog has been modified*/
	{desktopDamagedErr, mgArgErr }, /*desktop database files are corrupted*/
	{sameFileErr,  mgArgErr  },     /*can't exchange a file with itself*/
	{badFidErr,    mgArgErr  },     /*file id is dangling or doesn't match with the file number*/
	{afpRangeOverlap, fNoPerm },    /*locking error*/
	{afpRangeNotLocked, fNoPerm },  /*unlocking error*/
	{afpObjectTypeErr, fNoPerm },   /*file is a directory*/
	{afpAccessDenied, fNoPerm },    /*user does not have correct acces to the file*/
};

static MgErr MacToLVErr(OSErr osErr)
{
	int i;

	/* check the table for the OS error code */
	for (i = 0; i < ERRTABLESIZE; ++i)
	{
		if (osErr == errtable[i].maccode)
		{
			return errtable[i].err;
		}
	}

	/* The error code wasn't in the table. */
	return bogusError;
}
#endif