#include "extcode.h"

/* Magic Cookies */
typedef uInt32 MagicCookie;

struct MagicCookieJarStruct;
typedef struct MagicCookieJarStruct MCJar, **MagicCookieJar;
typedef UPtr MagicCookieInfo;

#define kNotAMagicCookieJar ((MagicCookieJar) 0)

typedef struct {
	int32		count;		/* number of cookies that follow */
	MagicCookie cookie[1];	/* cookies */
} MagicCookieList, *MagicCookieListPtr, **MagicCookieListHandle;

MagicCookieJar MCNewBigJar(uInt32 size);
MgErr MCDisposeJar(MagicCookieJar);
MagicCookie MCNewCookie(MagicCookieJar, MagicCookieInfo);
MgErr MCDisposeCookie(MagicCookieJar, MagicCookie, MagicCookieInfo);
MgErr MCGetCookieInfo(MagicCookieJar, MagicCookie, MagicCookieInfo);
MgErr MCGetCookieInfoPtr(MagicCookieJar, MagicCookie, MagicCookieInfo*);
MgErr MCGetCookieList(MagicCookieJar, MagicCookieListHandle*);
Bool32 MCIsACookie(MagicCookieJar, MagicCookie);

/* Runtime Cleanup support */
enum {		/* cleanup modes (when to call cleanup proc) */
	kCleanRemove,
	kCleanExit,				/* only when LabVIEW exits */
	kCleanOnIdle,			/* whenever active vi goes idle */
	kCleanAfterReset,		/* whenever active vi goes idle after a reset */
	kCleanOnIdleIfNotTop,	/* whenever active vi goes idle if active vi is not current vi */
	kCleanAfterResetIfNotTop/* whenever active vi goes idle after a reset if active vi is not current vi */
	};
typedef int32	(*CleanupProcPtr)(UPtr);
int32 RTSetCleanupProc(CleanupProcPtr, UPtr, int32);

#if defined(_DEBUG)
 #if Win32
  #if defined(_CVI_DEBUG_)
   #define DoDebugger()
  #elif _MSC_VER >= 1400
	 #define DoDebugger()   __debugbreak()
  #else
   #define DoDebugger()    {__asm int 3}
  #endif
 #elif MacOS
  #define DoDebugger()    Debugger()
 #else
  #define DoDebugger()
 #endif
 #define DEBUGPRINTF(args)      DbgPrintf args
#else
 #define DoDebugger()
 #define DEBUGPRINTF(args)
/* long DebugPrintf(char* fmt, ...);
 long DebugPrintf(char* fmt, ...)
 {
   return 0;
 }
*/
#endif

#if defined(PIPES_EXPORTS)
#define LibAPI(type) __declspec(dllexport) type __cdecl
#else
#define LibAPI(type) __declspec(dllimport) type __cdecl
#endif

typedef enum
{
	kNone,
	kReadMode,
	kWriteMode,
	kReadWriteMode
} Modes;

LibAPI(MgErr) PipeOpen(CStr name, uInt16 mode, LVRefNum *refnum);
LibAPI(MgErr) PipeOpenCmd(CStr command, uInt16 mode, LVRefNum *refnumIn, LVRefNum *refnumOut,
                         LVRefNum *refnumErr, uInt32 *processID);
LibAPI(MgErr) PipeOpenCmdAsUser(CStr username, CStr password, CStr command, uInt16 mode,
							   LVRefNum *refnumIn, LVRefNum *refnumOut, LVRefNum *refnumErr, uInt32 *processID);
LibAPI(MgErr) PipeClose(LVRefNum *refnum);
LibAPI(MgErr) PipeRead(LVRefNum *refnum, uInt32 offset, uInt32 *bytesRead, uInt16 mode, LStrHandle data, uInt32 *eof, LVRefNum *context);
LibAPI(MgErr) PipeWrite(LVRefNum *refnum, uInt32 offset, uInt32 *bytesWritten, LStrHandle data);
LibAPI(MgErr) InitializeStandardIO(uInt32 processID, LVRefNum *refnumIn, LVRefNum *refnumOut, LVRefNum *refnumErr);

LibAPI(MgErr) ExecuteCommand(CStr command, CStr verb, CStr param, uInt32 *processID); 
LibAPI(MgErr) KillProcess(uInt32 pid, int32 exitCode);
