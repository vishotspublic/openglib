LabVIEW Pipe library, version 1.1.0
----------------------------------

Copyright 2002-2017 Rolf Kalbermatter



Release 1.1.0, Released: Oct. 05, 2017
======================================
First official release for a library which allows to support pipes in a LabVIEW application.
Pipes are communication channels which are well known from Unix systems. Under Unix standard-IO
of command line tools can be redirected to pipes and then used from other programs to read from
and write to them.

With this library this functionality is also available under Windows. In addition the library
also supports named pipes, which are sometimes used for interapplication communication.