-= oglib_pipe-1.2.0-1.ogp =-

[NEW] Added Open System Command As User.vi
[MOD] Change the implementation of the refnum in the DLL so it also works for 64 bit

-= oglib_pipe-1.0-1.ogp =-

[NEW] Added Kill Process,vi function
[MOD] changed Create Commmand process to allow for no pipes to open to
      create only a process that can be killed later through its ID. 

