[Package Name]
Name=oglib_pipe
Version=1.0
Release=1

[Description]
Description="The pipe package contains several routines for supporting pipe communication.\0D\0AThis is typically used for interprocess communication. It also has a function start an external command line utility and redirect its standard IO to pipes to use them for communication with that utility from within LabVIEW."
Summary="OpenG Pipe Tools"
License=LGPL
Copyright="2004 - 2006 Rolf Kalbermatter"
Distribution="OpenG Toolkit"
Icon=ogpipe.bmp
Vendor=OpenG.org
URL=http://openg.org/ogpipe
Packager="Jim Kring <jim@jimkring.com>"

[Script VIs]

[Dependencies]
AutoReqProv=FALSE

[Platform]
Exclusive_LabVIEW_Version= >=7.0
Exclusive_LabVIEW_System=All
Exclusive_OS=All

[Files]
Num File Groups=5

[File Group 0]
Source Dir=built/ogpipe/ogpipe.llb
Target Dir=<user.lib>/_OpenG.lib/ogpipe/ogpipe.llb
Replace Mode=Always

Num Files=9

File 0=OGPIPE - VI Tree__ogtk.vi
File 1=OGPIPE Close Pipe__ogtk.vi
File 2=OGPIPE Execute System Command__ogtk.vi
File 3=OGPIPE Kill Process__ogtk.vi
File 4=OGPIPE Open Pipe__ogtk.vi
File 5=OGPIPE Open System Command__ogtk.vi
File 6=OGPIPE Read From Pipe__ogtk.vi
File 7=OGPIPE RefNum__ogtk.ctl
File 8=OGPIPE Write To Pipe__ogtk.vi

[File Group 1]
Source Dir=built/ogpipe
Target Dir=<user.lib>/_OpenG.lib/ogpipe
Exclusive_OS=Windows NT, Windows 9x
Replace Mode=Always
Num Files=2
File 0=ogpipes.dll
File 0=ogpipes64.dll

[File Group 2]
Source Dir=built/ogpipe
Target Dir=<user.lib>/_OpenG.lib/ogpipe
Exclusive_OS=Linux
Replace Mode=Always
Num Files=1
File 0=ogpipes.so
File 0=ogpipes64.so

[File Group 1]
Source Dir="Dynamic Palette MNUs"
Target Dir="<user.lib>/_dynamicpalette_dirs/comm"
Replace Mode=Always
Num Files=1
File 0=oglib_pipe.mnu

[File Group 2]
Source Dir="Dynamic Palette MNUs"
Target Dir="<user.lib>/_dynamicpalette_dirs/file"
Replace Mode=Always
Num Files=1
File 0=oglib_pipe.mnu
